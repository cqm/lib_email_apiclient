<?php declare(strict_types=1);

namespace CQM\Libraries\Email\ApiClient\Exception;

class ApiClientException extends \Exception {}
