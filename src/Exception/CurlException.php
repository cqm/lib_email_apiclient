<?php declare(strict_types=1);

namespace CQM\Libraries\Email\ApiClient\Exception;

class CurlException extends ApiClientException
{
    public function __construct($message, $code, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
