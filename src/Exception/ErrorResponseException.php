<?php declare(strict_types=1);

namespace CQM\Libraries\Email\ApiClient\Exception;

use CQM\Libraries\Email\ApiClient\Http\Response;

class ErrorResponseException extends ApiClientException
{
    /**
     * @var Response
     */
    private $response;

    public function __construct(Response $response, \Throwable $previous = null)
    {
        parent::__construct($response->getPlainResponse(), $response->getHttpStatusCode(), $previous);

        $this->response = $response;
    }

    /**
     * Returns the response
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }
}
