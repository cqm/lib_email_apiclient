<?php declare(strict_types=1);

namespace CQM\Libraries\Email\ApiClient\Util;

class UtilMessage
{
    const INVALID_AUTH_CREDENTIALS = 'Invalid API credentials. (null given)';
}
