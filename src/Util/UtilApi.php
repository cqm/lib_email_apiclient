<?php declare(strict_types=1);

namespace CQM\Libraries\Email\ApiClient\Util;

use CQM\Libraries\Security\Signature;

class UtilApi
{
    const TOKEN_SEPARATOR = '#';

    public static function generateToken($apiKey, $apiSecret) : string
    {
        $signature = Signature::makeHmacSha256($apiSecret, $nonce, $ts);

        return implode(self::TOKEN_SEPARATOR, array($apiKey, $nonce, $ts, $signature));
    }
}
