# CQM Email REST API Client

### REST client to make requests to CQM APIs programmatically.

The ApiClient() class provides encapsulation for HTTP verbs:

**get(*string* $endpoint, *array* $params)**

**post(*string* $endpoint, *array* $params)**

**put(*string* $endpoint, *array* $params)**

...

It also handles API authentication. Include your API keys when creating the ApiClient instance.

Basic example:

```php
<?php

use CQM\Libraries\Email\ApiClient\ApiClient;

$apiKey = 'your api key';
$apiSecret = 'your api secret';
$baseUri = 'https://example.com';

// Create a client instance
$apiClient = new ApiClient($apiKey, $apiSecret, $baseUri);

try {
    // Edit one template 
    $response = $apiClient->put('/api/v1/templates', array(
        'application_id' => 1,
        'template_id' => 1,
        'subject' => 'Edited from API client',
        'body' => 'My content'
    ));
    
    echo($response->getPlainResponse());

} catch (\Exception $ex) {
    print($ex->getMessage());
}
```

